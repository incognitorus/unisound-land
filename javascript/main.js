function custom_attributes(xloc, yloc, value, total, R) {
    var alpha = 360 / total * value,
        a = (90 - alpha) * Math.PI / 180,
        x = xloc + R * Math.cos(a),
        y = yloc - R * Math.sin(a),
        path;
    if (total == value) {
        path = [
            ["M", xloc, yloc - R],
            ["A", R, R, 0, 1, 1, xloc - 0.01, yloc - R]
            ];
    } else {
        path = [
            ["M", xloc, yloc - R],
            ["A", R, R, 0, +(alpha > 180), 1, x, y]
            ];
    }
    return {
        path: path
    };
};

var circle_settings = {
    "stroke": "#5a5a5a",
    "stroke-width": 30
}

var arc_settings = {
    "stroke": "#00a9e2",
    "stroke-width": 30,
    arc: [125, 125, 0, 100, 110]
}

//text in the middle
var text_settings = {
    "font-size": 14,
    "fill": "#000",
    "font-family": "museo_sans_cyrl500"
}

function get_changed_paper_text(param) {
  return "Исследования\n Forrester показали,\n что " + param + "% слушателей\n вспоминают, что слышали\n in-stream audio ролик\n в течение последних\n 30 дней"
}

function get_changed_paper2_text(param) {
  return "Из этих слушателей\n " + param + "% отвечают\n на рекламу тем\n или иным способом"
}

function onAnimatePaper() {
  var howMuch = paper.theArc.attr("arc");
  paper.textObject.attr("text", paper.text_funct( Math.floor(howMuch[2])) );
}

function onAnimatePaper2() {
  var howMuch = paper2.theArc.attr("arc");
  paper2.textObject.attr("text", paper2.text_funct( Math.floor(howMuch[2])) );
}

function Paper(params) {
  this.class_name = params.class_name;
  this.amount = params.amount;
  this.paper = false;
  this.backCircle = false;
  this.theArc = false;
  this.text = params.text;
  this.textObject = false;
  this.text_funct = params.text_funct;
  this.on_animate_funct = params.on_animate_funct;
}

Paper.prototype.init = function() {
  this.paper = Raphael(this.class_name, 250, 250);
  this.paper.customAttributes.arc = custom_attributes;
  this.paper.circle(125, 125, 110).attr(circle_settings);
  this.theArc = this.paper.path().attr(arc_settings);
  eve.on("raphael.anim.frame.*", this.on_animate_funct);
  this.textObject = this.paper.text(125, 125, this.text)
    .attr(text_settings);
}

Paper.prototype.reset = function() {
  this.paper.remove()
  this.init();
}

Paper.prototype.onAnimate = function(paper) {
    var howMuch = paper.theArc.attr("arc");
    paper.textObject.attr("text", paper.text_funct( Math.floor(howMuch[2])) );
}

Paper.prototype.animate = function() {
  var paper = this;
  this.theArc.rotate(0, 100, 100).animate({
    arc: [125, 125, this.amount, 100, 110]
  }, 600, function() {
    //when the animation is done unbind
    eve.unbind("raphael.anim.frame.*", paper.on_animate_funct);
  });
}

var paper_setiings = {
  class_name: "efficiency-in-stream-month",
  amount: 80,
  text_funct: get_changed_paper_text,
  on_animate_funct: onAnimatePaper,
  text: "Исследования\n Forrester показали,\n что 0% слушателей\n вспоминают, что слышали\n in-stream audio ролик\n в течение последних\n 30 дней"
}

var paper2_setiings = {
  class_name: "efficiency-in-stream-answer",
  amount: 44,
  text_funct: get_changed_paper2_text,
  on_animate_funct: onAnimatePaper2,
  text: "Из этих слушателей\n 0% отвечают\n на рекламу тем\n или иным способом"
}

var paper = new Paper(paper_setiings)
var paper2 = new Paper(paper2_setiings)

paper.init();
paper2.init();












function efficiency_diagramm_animate() {
  //the animated arc
  paper.animate()
  paper2.animate()
}

function efficiency_graph_animate() {
  $('div.efficiency-graph-scale-top,div.efficiency-graph-scale-bottom').each(function() {
    $(this).width($(this).data('width'));
    var graph_object_dom = $(this).find('span')
    counter_loop(600, parseInt($(this).data('percent')), function(index) {
      graph_object_dom.html(Math.floor(index) + "%");
    })
  });
}

function counter_loop(time, max, callback) {
  var current_circle = 1;
  var step = 50
  count_circle = time/step;
  
  var timer = setInterval(function() {
    callback((max/count_circle)*current_circle)
    if (current_circle >= count_circle) {
      clearInterval(timer)
    } else {
      current_circle += 1;
    }
  }, step)
}



function run_efficiency_animate() {
  if (! efficiency_run_trigger) {
    efficiency_diagramm_animate();
    efficiency_graph_animate();
    efficiency_run_trigger = true;
  }
}

function clear_efficiency_animate() {
  paper.reset();
  paper2.reset();

  $('div.efficiency-graph-scale-top,div.efficiency-graph-scale-bottom').each(function() {
    $(this)
      .width(0)
      .find('span').text("0%");
  })

  efficiency_run_trigger = false;
}







$(function() {
  efficiency_run_trigger = false;

  $(window).on('scroll', function() {
    scroll_top = $(window).scrollTop();

    if (scroll_top%2 === 0) {
      // scroll menu
      if (scroll_top > 842) {
        if (! $('nav').hasClass('fixed')) {
          $('nav').addClass('fixed')
        }
      } else {
        if ($('nav').hasClass('fixed')) {
          $('nav').removeClass('fixed')
        }
      }

      //scroll efficiency
      if (scroll_top >= 3430) {
        run_efficiency_animate();
      }

      //celar efficiency trigger
      if (scroll_top < 2500) {
        if (efficiency_run_trigger)
          clear_efficiency_animate();
      }
    }
  });

  //click menu
  $('nav p a').on('click', function(e) {
    e.preventDefault();
    var section_name = $(this).attr('class').slice(9);

    $('html, body').animate({
        scrollTop: $("#" + section_name).offset().top
    }, 500, function() {
      if (section_name === "efficiency") {
        run_efficiency_animate();
      }
    });
  });

  //click example

  $('section.services div.services-item p a').on('click', function(e) {
    e.preventDefault();
    var link = $(this);

    if (! link.parent().next().hasClass('hidden')) {
      return false;
    }

    $('section.video').slideUp(300, function() {
      $('section.video div.video-player div')
        .addClass('hidden');
      
      $('section.video div.video-player div.video-' + link.attr('class').slice(19))
        .removeClass('hidden');

      $('section.video').slideDown(300);

      $('section.services div.services-item div.services-item-arrow').addClass('hidden');
      link.closest('div.services-item').find('div.services-item-arrow').removeClass('hidden');
    })
  });

  //scroll efficiency
  if ($(window).scrollTop() >= 3430 && ! efficiency_run_trigger) {
    run_efficiency_animate();
  }

  //download brochure
  $('p.excellence-download a.button').on('click', function(e) {
    if (! $(this).data('first-click')) {
      e.preventDefault();
      $(this).data('first-click', 'addf')
      $('div.excellence-mailing').slideDown(200);
    }
  });

  $('p.benefits-join a,p.otp-join a').on('click', function(e) {
    e.preventDefault();
    $('#partners-form').show();

    $('html, body').animate({
        scrollTop: $("#partners-form").offset().top
    }, 500);
  });
})